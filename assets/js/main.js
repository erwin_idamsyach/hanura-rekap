const router = new VueRouter({
  mode: "history",
  base: VUE_BASE_PATH,
  routes: [
    { path: "/not-found", component: NOTFOUND },
    { path: "/dashboard_rekap", component: DASHBOARD },
    { path: "/master/dapil", component: MASTER_DAPIL },
    { path: "/master/pemilihan", component : PEMILIHAN },
    { path: "/master/wilayah", component : MASTER_WILAYAH },
    { path: "/master/caleg", component : MASTER_CALEG },
    { path: "/input-suara", component : INPUT_SUARA },
    { path: "/input-suara-dapil", component : INPUT_SUARA_DAPIL },
    { path: "*", redirect: "/not-found" }
  ]
});

router.beforeEach(function(to, from, next) {
  NProgress.start();
  NProgress.done();
  next();
});

const vm = new Vue({
  router,
  store,
  el: "#app_ml",
  data: {
    msg: "Hello World",
    machine: [],
    active_action: null
  },
  computed: {
    background: function() {
      return this.$store.state.theme.sidebar;
    },
    menu_active: function() {
      return this.$store.state.theme.menu_active;
    },
    title: function() {
      return this.$store.state.crumbs;
    },
    name: function() {
      return this.$store.state.data_login.employee_name;
    },
    role_id: function() {
      return this.$store.state.data_login.role;
    },
    menu: function(argument) {
      return this.$store.state.menu;
    },
    sub_menu: function(argument) {
      return this.$store.state.sub_menu;
    }
  },
  methods: {
    get_active_action: function(id_sub_menu, callback) {
      var v = this;

      $.ajax({
        type: "POST",
        url: BASE_URL + "menu/action_sub_menu",
        data: { id_sub_menu: id_sub_menu, role_id: v.role_id },
        success: function(resp) {
          v.$set(v, "active_action", resp);
          callback(resp);
        },
        error: function(e) {
          alert("Something wrong!");
          console.log(e);
        }
      });
    },
    rupiah: function convertToRupiah(angka) {
      if (parseInt(angka) != 0) {
        var angka = angka.toString().replace(".00", "");
        var rupiah = "";
        var angkarev = angka
          .toString()
          .split("")
          .reverse()
          .join("");
        for (var i = 0; i < angkarev.length; i++)
          if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + ".";
        return (
          "Rp. " +
          rupiah
            .split("", rupiah.length - 1)
            .reverse()
            .join("") +
          ",00"
        );
      } else {
        return "0";
      }
    },
    getMenu: function() {},
    getPicturePorfile: function() {
      if (this.$store.state.data_login.pic_path != null) {
        return (
          BASE_URL +
          "assets/img/profile_pic/" +
          this.$store.state.data_login.pic_path
        );
      } else {
        return BASE_URL + "assets/img/faces/card-profile1-square.jpg";
      }
    },
    showLoading: function(text) {
      swal({
        title: text,
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
          swal.showLoading();
        }
      });
    },
    showSuccess: function(text, callback) {
      swal({
        title: text,
        type: "success",
        timer: 2000,
        showConfirmButton: false,
        onClose: function() {
          if (callback != undefined) {
            callback();
          }
        }
      });
    },
    showError: function(text) {
      swal({
        title: text,
        type: "error",
        timer: 2000,
        showConfirmButton: false,
        onClose: function() {}
      });
    },
    showConfirmation: function(title, text, callback) {
      swal({
        title: title,
        text: text,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        dangerMode: true
      }).then(function(isConfirm) {
        if (isConfirm.value == true) {
          callback();
        }
      });
    },
    dropifyInit: function() {
      $(".dropify").dropify();
    },
    logout: function() {
      $.get(BASE_URL + "login/logout", function(data) {
        window.location = BASE_URL + "login";
      });
    },
    goToProfile: function() {
      this.$router.push({ path: "profile-user", query: { mode: "view" } });
    }
  },
  updated: function() {
    $(".sidebar .sidebar-wrapper, .main-panel").scrollTop(0);
  },
  mounted: function() {
    const v = this;
    $(".sidebar .sidebar-wrapper, .main-panel").perfectScrollbar("destroy");
    $(".sidebar .sidebar-wrapper, .main-panel").perfectScrollbar();
  },
  beforeCreate: function() {

  },
  created: function() {
    this.$store.dispatch("setData");
  }
});
