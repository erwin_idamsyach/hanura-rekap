<?php

class Api extends CI_Controller{

	function __Construct(){
		parent::__Construct();

		$this->load->model('Api_model');
	}

	public function index(){
		echo "index";
	}

	public function getMenuList(){
		$arr = array();
		$role_id = $this->session->userdata('sess_role_id');

		$menu = $this->Api_model->getMenu($role_id);
		$sub_menu = $this->Api_model->getSubMenu($role_id);

		$arr['menu'] = $menu->result();
		$arr['sub_menu'] = $sub_menu->result();

		echo toJson($arr);

	}

	public function getYearList(){
		echo toJson(getYearList());
	}	

	public function getWeekList(){
		echo getWeekList();
	}

	public function getPemilihan(){
		$arr = [];
		$getData = $this->db->query("SELECT * FROM m_type_pemilihan ORDER BY tipe_pemilihan ASC");
		$arr['data'] = $getData->result();
		echo setJson($arr);
	}

	public function getDapil(){
		$arr = [];
		$id_pemilihan = (null !== $this->input->get('id_pemilihan'))?$this->input->get('id_pemilihan'):null;

		if($id_pemilihan != null){
			if($id_pemilihan == 1){
				$getData = $this->db->query("SELECT 
					m_dapil.*, 
					m_type_pemilihan.tipe_pemilihan 
					FROM 
					m_dapil 
					INNER JOIN m_type_pemilihan ON m_dapil.id_pemilihan = m_type_pemilihan.id
					WHERE 
					id_pemilihan=$id_pemilihan
					ORDER BY 
					nama_dapil ASC");
			}else if($id_pemilihan == 2){
				$id_prov = $this->input->get('id_prov');

				$getData = $this->db->query("SELECT 
									m_dapil.*, 
									m_type_pemilihan.tipe_pemilihan 
									FROM 
									m_dapil 
									INNER JOIN m_type_pemilihan ON m_dapil.id_pemilihan = m_type_pemilihan.id
									WHERE 
									id_pemilihan=$id_pemilihan
									AND 
									id_prov = $id_prov
									ORDER BY 
									nama_dapil ASC");
			}else if($id_pemilihan == 3){
				$id_prov = $this->input->get('id_prov');
				$id_kab = $this->input->get('id_kab');

				$getData = $this->db->query("SELECT 
									m_dapil.*, 
									m_type_pemilihan.tipe_pemilihan 
									FROM 
									m_dapil 
									INNER JOIN m_type_pemilihan ON m_dapil.id_pemilihan = m_type_pemilihan.id
									WHERE 
									id_pemilihan=$id_pemilihan
									AND 
									id_prov = $id_prov
									AND 
									id_kota = $id_kab
									ORDER BY 
									nama_dapil ASC");
			}else{

			}
		}else{
			$getData = $this->db->query("SELECT 
					m_dapil.*, 
					m_type_pemilihan.tipe_pemilihan 
					FROM 
					m_dapil 
					INNER JOIN m_type_pemilihan ON m_dapil.id_pemilihan = m_type_pemilihan.id
					ORDER BY 
					nama_dapil ASC");
		}

		$arr['data'] = $getData->result();

		echo setJson($arr);
	}

	public function getKotaList(){
		$arr = [];
		$id_dapil = (null !== $this->input->get('id_dapil'))?$this->input->get('id_dapil'):null;

		if($id_dapil != null){
			$getData = $this->db->query("SELECT 
					detail_dapil.*, 
					m_dapil.nama_dapil, 
					m_type_pemilihan.tipe_pemilihan
			FROM detail_dapil
			INNER JOIN m_dapil ON detail_dapil.id_dapil = m_dapil.id
			INNER JOIN m_type_pemilihan ON m_dapil.id_pemilihan = m_type_pemilihan.id
			WHERE detail_dapil.id_dapil = $id_dapil
			ORDER BY 
			nama_daerah ASC
			");
		}else{
			$getData = $this->db->query("SELECT 
					detail_dapil.*, 
					m_dapil.nama_dapil, 
					m_type_pemilihan.tipe_pemilihan
			FROM detail_dapil
			INNER JOIN m_dapil ON detail_dapil.id_dapil = m_dapil.id
			INNER JOIN m_type_pemilihan ON m_dapil.id_pemilihan = m_type_pemilihan.id
			ORDER BY 
			nama_daerah ASC
			");
		}

		$arr['data'] = $getData->result();

		echo setJson($arr);
	}

	public function getCalegList(){
		$arr = [];

		$id_dapil = (null !== $this->input->get('id_dapil'))?$this->input->get('id_dapil'):null;
		$id_partai = (null !== $this->input->get('id_partai'))?$this->input->get('id_partai'):null;

		if($id_dapil != null){
			$getData = $this->db->query("SELECT 
					tb_calon_list.*, 
					m_dapil.nama_dapil, 
					m_type_pemilihan.tipe_pemilihan
			FROM tb_calon_list
			INNER JOIN m_dapil ON tb_calon_list.id_dapil = m_dapil.id
			INNER JOIN m_type_pemilihan ON m_dapil.id_pemilihan = m_type_pemilihan.id
			WHERE tb_calon_list.id_dapil = $id_dapil
			AND tb_calon_list.id_partai = $id_partai
			");
		}else{
			$getData = $this->db->query("SELECT 
					tb_calon_list.*, 
					m_dapil.nama_dapil, 
					m_type_pemilihan.tipe_pemilihan
			FROM tb_calon_list
			INNER JOIN m_dapil ON tb_calon_list.id_dapil = m_dapil.id
			INNER JOIN m_type_pemilihan ON m_dapil.id_pemilihan = m_type_pemilihan.id
			");
		}

		$arr['data'] = $getData->result();

		echo setJson($arr);
	}

	public function getProv(){
		$arr = [];
		$getData = $this->db->query("SELECT * FROM ms_provinsi");
		$arr['data'] = $getData->result();

		echo setJson($arr);
	}

	public function getKabKota(){
		$arr = [];

		$id_prov = (null !== $this->input->get('id_prov'))?$this->input->get('id_prov'):null;
	}

	public function getPartai(){

		$arr = [];

		$getData = $this->db->query("SELECT * FROM ms_party ORDER BY no_urut ASC");
		$arr['data'] = $getData->result();

		echo setJson($arr);

	}

	public function get_dapil_votes(){
		$arr = [];

		$id_dapil = $this->input->get("id_dapil");

		$partai = $this->db->query("SELECT * FROM ms_party ORDER BY no_urut ASC");

		$partai_list = [];
		foreach($partai->result() as $par){
			array_push($partai_list, [
				"id_dapil" => $id_dapil,
				"id_partai" => $par->id,
				"nama_partai" => $par->nama_partai,
				"total_suara" => (int)0
			]);
		}

		$getVotes = $this->db->query("SELECT * FROM tb_dapil_votes WHERE id_dapil=$id_dapil");
		if($getVotes->num_rows() > 0){
			foreach($getVotes->result() as $v){
				if(null !== $v->id_partai){
					$key = array_search($v->id_partai, array_column($partai_list, "id_partai"));

					if(null !== $key){
						$partai_list[$key]['total_suara'] = (int)$v->total_suara;
					}

				}else{
					continue;
				}
			}
		}

		$getTotal = $this->db->query("SELECT SUM(total_suara) as SUARA_DAPIL FROM tb_dapil_votes WHERE id_dapil=$id_dapil")->row();

		$arr['status'] = "success";
		$arr['code'] = 200;
		$arr['data']['id_dapil'] = $id_dapil;
		$arr['data']['list'] = $partai_list;
		$arr['data']['suara_dapil'] = $getTotal->SUARA_DAPIL;

		echo setJson($arr);

	}

	public function save_votes_dapil(){
		$arr = [];
		$voteList = $this->input->post('voteList');

		foreach($voteList as $vl){

			$id_dapil = $vl['id_dapil'];
			$id_partai = $vl['id_partai'];
			$total_suara = $vl['total_suara'];

			$cek = $this->db->query("SELECT id FROM tb_dapil_votes WHERE id_dapil=$id_dapil AND id_partai=$id_partai");

			if($cek->num_rows() == 1){
				$this->db->where('id_dapil', $id_dapil)
							->where('id_partai', $id_partai)
							->update('tb_dapil_votes', [
								"total_suara" => $total_suara
							]);
			}else{
				$this->db->insert('tb_dapil_votes', [
					"id_dapil" => $id_dapil,
					"id_partai" => $id_partai,
					"total_suara" => $total_suara
				]);
			}
		}

		if($this->db->affected_rows() > 0){
			$arr['status'] = "success";
		}else{
			$arr['status'] = "failed";
		}

		echo setJson($arr);

	}

	public function getKokab(){
		$id_prov = $this->input->get('id_prov');

		$getData = $this->db->where('id_prov', $id_prov)
							->get('ms_kab_kota');

		echo setJson($getData->result());
	}

}

?>